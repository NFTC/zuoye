#include <stdio.h>
#include <string.h>

int encode(char*s)
{
    int num=strlen(s);
//字母左移3位
    for(int i=0; i<num; i++)
    {
        if(s[i]>='a' && s[i]<='z')
        {
            s[i]=(s[i]-'a'-3)%26+'a';
        }else if(s[i]>='A' && s[i]<='Z')
        {
            s[i]=(s[i]-'A'-3)%26+'A';
        }
    }

//逆序存储
    int i=0,j=num-1;
    while(i<j)
    {
        char t=s[i];
        s[i]=s[j];
        s[j]=t;
        i++;
        j--;
    }
//大小写转换ת
    for(int i=0; i<num; i++)
    {
        if(s[i]>='a' && s[i]<='z')
            s[i]+='A'-'a';
        else if(s[i]>='A' && s[i]<='Z')
            s[i]+='a'-'A';
    }

    return 0;
}
int main()
{
    char s[60]="";
    gets(s);
    encode(s);
    puts(s);
    return 0;
}
