#include<iostream>
#include<math.h>
#include<string.h>
using namespace std;
char *intToString(int x){
	int number=1;
//判断ｘ是几位数 
    int x1=x;
	while((x1/10)>0){
		number++;
		x1=x1/10;
	}
	char *newStr=new char[number+1];
	for(int i=0;i<number;i++){
		newStr[i]=x%10+'0';//因为０表示空，所以把他变成字符 
		x=x/10;
	}
	newStr[number]='\0';
	return newStr;	
}
//将字符窜每一位相加 
int stringToInt(char*str){
	int length=strlen(str);
	double sum=0;
	for(int i=0;i<length;i++){
		sum+=str[i]-'0';
	}
	return sum;
}
int add(int n){
	 double Sum=0;
	 char*str;
	 for(int i=1;i<=n;i++){
	 	str=intToString(i);
	 	Sum+=stringToInt(str);
	 }
	 return Sum;
}
int main(){
	int n;
	cout<<"请输入n"<<endl;
	cin>>n;
	cout<<add(n);
}
