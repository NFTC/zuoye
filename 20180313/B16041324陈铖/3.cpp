#include<stdio.h>

int getSum(int n)
{
     int i=0;
     int j=0;
     int temp=0;
     int sum=0;
     for(i=0;i<=n;i++)
     {
        temp=0;
        j=i;
        while(j)
        {
            temp+=j%10;
            j=(int)(j/10);
        }
        sum+=temp;
     }
    return sum;
}

int main()
{
    int n = -1;
    int sum;
    while (n < 0)
    {
        scanf("%d", &n);
    }
    sum = getSum(n);
    printf("%d\n", sum);
    return 0;
}
