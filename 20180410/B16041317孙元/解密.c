#include <stdio.h>
#include <string.h>
int main()
{
    int i;
    char in[100] = "";
    int length = 0;
    char temp;
    gets(in);
    length = strlen(in);
    for (i = 0; in[i] != '\0'; i++)
    {
        if ('A' <= in[i] && in[i] <= 'Z')
            in[i] = (in[i] + 3 - 'A') % 26 + 'a';
        else if ('a' <= in[i] && in[i] <= 'z')
            in[i] = (in[i] + 3 - 'a') % 26 + 'A';
    }
    for (i = 0; i < length / 2; i++)
    {
        temp = in[i];
        in[i] = in[length - 1 - i];
        in[length - 1 - i] = temp;
    }
    puts(in);
    return 0;
}
