#include<stdio.h>
#include<string.h>
char strlwr(char a)
{
	char x=a;
	if(x>='a'&&x<='z')
	{
		x=(x+3-'a')%26+'a';
		x=x-32;
	}
	else if(x>='A'&&x<='Z')
	{
		x=(x+3-'A')%26+'A';
		x=x+32;
	}
	return x; 
}
void Swap(char str[20],int k)
{
	int i,temp;
	for(i=0;i<k/2;i++)
	{
		temp=str[i];
		str[i]=str[k-1-i];
		str[k-i-1]=temp;	
	}
}
int main()
{
	char str[20];
	int i=0,k;
	for(i=0;i<20;i++)
		str[i]='A'; 
	i=0;//初始化，便于观察
	printf("请输入密令：") ;
	gets(str);
	k=strlen(str);
	Swap(str,k);
	printf("解密后："); 
	while(i<k)
	{
		str[i]=strlwr(str[i]);
		putchar(str[i]);
		i++;
	}
		return 0;
}
