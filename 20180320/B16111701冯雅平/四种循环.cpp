#include<iostream>
using namespace std;
void f(int x);
 void w(int x);
 void dw(int x);
  void w1(int x);
int main()
{
       cout<<"Please input the number:"<<endl;
       int x;
       cin>>x;
       f(x);
       w(x);
       dw(x);
       w1(x);
       return 0;
}
void f(int x)   //for循环
{
       double sum=0;
       for(;x;x=x/10)
              sum+=x%10;
       cout<<"for循环结果为："<<sum<<endl;
       return ;
}
 void w(int x)//while   循环
 {
        double sum=0;
        while(x)
        {
               sum+=x%10;
               x=x/10;
        }
        cout<<"while循环的结果为： "<<sum<<endl;
        return ;
 }
 void dw(int x)//do-while循环
 {
        double sum=0;
        do{
              sum+=x%10;
              x=x/10;
        }while(x);
        cout<<"do-while循环的结果为："<<sum<<endl;
        return;
 }
 void w1(int x)// while(1)循环
 {
        double sum=0;
        while(1){
              sum+=x%10;
              x=x/10;
              if(!x)
                     break;
        }
        cout<<"while(1)循环的结果为："<<sum<<endl;
        return;
 }
