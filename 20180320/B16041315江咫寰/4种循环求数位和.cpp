#include<iostream>
using namespace std;

int A(int n)            //利用while循环求数位和
{
    int sum=0;
    int count=0;
    while(n!=0)
    {
        count=n%10;
        sum+=count;
        n=n/10;
    }
    return sum;
}

int B(int n)            //利用do-while循环求数位和 
{
    int sum=0;
    int count=0;
    do
    {
        count=n%10;
        sum+=count;
        n=n/10;
    }while(n>1);
    return sum;
}

int C(int n)           //利用for循环求数位和
{
	int sum=0;
	int count=0;
	for(;n>1;n/=10)
	{
		count=n%10;
		sum+=count;
	}
	return sum;
} 

int D(int n)          //利用while(1)循环求数位和 
{
	int sum=0;
	int count=0;
	while(1)
	{
		count=n%10;
		sum+=count;
		n/=10;
		if(!count)
			break;
	}
	return sum;
}

int main()         //编写一个main函数来调用上述函数，检验是否正确； 
{
    int m,n;
    cout<<"please input a number."<<endl;
    cin>>n;
    m=D(n);
    cout<<m<<endl;
    return 0;
}
