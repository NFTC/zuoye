#include <stdio.h>  
#include <math.h>  
int isPrime(int i)
{  
    int j=1;  
    for(int k=2;k<=sqrt(i);k++)
	{  
        if(i%k==0)
		{  
            j=0;  
            break;  
        } 
    }
    return j;  
}
int main()  
{  
    int count=0;  
    for(int i=1;i<1000;i++)
	{  
        if(isPrime(i))
		{  
            printf("%6d",i);  
            count++;  
            if(count%5==0)  
                printf("\n");     
        }  
        if(i>1000)  
            printf("\n");  
    }  
    return 0;  
} 

